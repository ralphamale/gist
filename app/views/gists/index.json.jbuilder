json.array! @gists do |gist|
  json.id gist.id
  json.title gist.title
  json.user_id gist.user_id

  json.gist_files do
    json.array! gist.gist_files do |file|
      json.id file.id
      json.name file.name
      json.body file.body
    end
  end
  json.favorite do
    if current_user.has_favorite?(gist)
      favorite = current_user.has_favorite?(gist)
      json.id favorite.id
    else
      json.null!
    end
  end
end