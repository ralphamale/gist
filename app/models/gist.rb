class Gist < ActiveRecord::Base
   attr_accessible :title, :user_id, :tag_ids
   has_many :favorites
   belongs_to :user

   has_many :gist_files,
   class_name: "GistFile",
   foreign_key: :gist_id,
   primary_key: :id

   has_many :tags, through: :taggings, source: :tag
end
