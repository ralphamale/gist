window.Gist = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {
    var gists = new Gist.Collections.Gists()
    gists.fetch({
      success: function(collection, responses){
        console.log("successful gists fetch");
        new Gist.Routers.Gists({gists: gists, rootEl: $('.content')});
        Backbone.history.start();
      }
    })
  }
};

