Gist.Views.GistDetail = Backbone.View.extend({

  template: JST['gists/show'],

  events: {
    'click button': 'toggleFavorite',
    'click button': 'addFavorite'
  },
  render: function(){
    var content = this.template({gist: this.model});
    this.$el.html(content);
    return this;
  },
  addFavorite: function() {
    var favorite = new Gist.Models.Favorite({gist: this.model, gist_id: this.model.id, user_id: this.model.get('user_id')})

    favorite.save(
      {success: function() {console.log("Saved")},
      error: function() {console.log("Error")}});

  }
  //
  // toggleFavorite: function(event){
  //
  //   var $button = $(event.currentTarget());
  //
  //
  //
  // }


});