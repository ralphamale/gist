Gist.Views.GistsIndex = Backbone.View.extend({
  tagName: 'ul',
  template: JST['gists/index'],

  render: function(){
    var content = this.template({gists: this.collection})
    this.$el.html(content);
    return this;
  }

});
