Gist.Views.Form = Backbone.View.extend({
  tagName: "form",
  className: "create-gist",
  template: JST["gists/form"],
  events: {
    "submit" : "createGist" //cant do .create-gist
  },
  render: function() {
    var content = this.template({model: this.model});
    this.$el.html(content);

    var gistFiles = this.model.get('gistFiles');


    if (this.model.isNew()) {
      var gfView = new Gist.Views.GistFileForm({model: new Gist.Models.GistFile()});
      this.$el.append(gfView.render().$el);


    }

    var that = this;
    gistFiles.models.forEach(function(gistFile) {
      var gfView = new Gist.Views.GistFileForm({model: gistFile});
      that.$el.append(gfView.render().$el);

    })

    // var gfView = new Gist.Views.GistFileForm({model: this.model});
    // this.$el.append(gfView.render().$el);
    return this;
  },
  createGist: function(event) {
    event.preventDefault();
    var gistFormData = $(event.target).serializeJSON()["gist"];

    this.model.set(gistFormData);

    function success() {
      Backbone.history.navigate("#", {trigger: true});
    }

    if (this.model.isNew()) {
      this.collection.create(this.model, {success: success})
    } else {
      this.model.save({}, {success: success});
    }


    //
    // var gistFileFormData = $(event.target).serializeJSON()["gistfile"];
    //
    // for (gistFileKey in gistFileFormData) {
    //   var newGF = new Gist.Models.GistFile(gistFileFormData[gistFileKey]);
    //
    //
    // }
    // var newGist = new Gist.Models.Gist(gistFormData);
    // newGist.save();
    // alert("Saved");

  }
})