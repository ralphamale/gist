Gist.Views.GistFileForm = Backbone.View.extend({
  template: JST["gists/fileform"],
  render: function() {
    var content = this.template({gistFile: this.model})
    this.$el.html(content);
    return this;
  }
})