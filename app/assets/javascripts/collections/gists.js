Gist.Collections.Gists = Backbone.Collection.extend({

  model: Gist.Models.Gist,
  url: '/gists'

});
