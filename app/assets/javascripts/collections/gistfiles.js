Gist.Collections.GistFiles = Backbone.Collection.extend({
  model: Gist.Models.GistFile,
  url: '/gist_files'
})