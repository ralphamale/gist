Gist.Models.Favorite = Backbone.Model.extend({
  initialize: function(options) {
    this.gist = options.gist
  },
  urlRoot: function() {
    return "/gists/" + this.gist.id + "/favorite";
  }
})