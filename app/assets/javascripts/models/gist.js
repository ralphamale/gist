Gist.Models.Gist = Backbone.Model.extend({
  urlRoot: '/gists',
  initialize: function() {
    this.favorite();
    this.gistFiles();
    console.log(this.attributes.favorite);
  },

  parse: function(response) {

    if (response["gist_files"]){
      this.gistFiles().set(response["gist_files"]); //this.GistFiles == []
      delete response["gist_files"];
    }


    if (response["favorite"]) {
      console.log(response["favorite"]);
      this.favorite().set(response["favorite"]);
      delete response["favorite"];
    } else {
      console.log("this is null");
      this.set({favorite: null});
      //this.favorite().set({id: null}); // {id: null}
      console.log("blah");
    }
    return response;
  },

  gistFiles: function(){
    if(!this.get('gistFiles')){
      var gistFiles = new Gist.Collections.GistFiles([], {gist: this});
      this.set({gistFiles: gistFiles});
    }
    return this.get('gistFiles');

  },

  favorite: function() {
    if (!this.get('favorite')) {
      var favoriteObj = new Gist.Models.Favorite({gist: this});
      this.set({favorite: favoriteObj});

            //
      //
      // favoriteObj.save({success: function() {
      //   that.set({favorite: favoriteObj});
      // }})

      //(gist) this.get('favorite') =

      // (gist) this.get('favorite').destroy();
      // (gist) this.set('favorite',null);

    }


   return this.get('favorite');
  },


  toJSON: function() {
    var newAttrs = _.extend({}, this.attributes);
    delete newAttrs["favorite"]

    return newAttrs;

  }
});
