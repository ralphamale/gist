Gist.Routers.Gists = Backbone.Router.extend({

  routes: {
    '': "index",
    "new": "new",
    ":id/edit": "edit",
    ':id': "show"
  },

  initialize: function(options) {
    this.gists = options.gists;
    this.$rootEl = options.rootEl;
  },

  index: function(){
    var view = new Gist.Views.GistsIndex({collection: this.gists});
    this._swapView(view);
  },

  new: function() {
    var view = new Gist.Views.Form({model: new Gist.Models.Gist(), collection: this.gists});
    this._swapView(view);
  },

  edit: function(id) {
    var view = new Gist.Views.Form({model: this.gists.get(id)});
    this._swapView(view);
  },

  show: function(id){
    var view = new Gist.Views.GistDetail({model: this.gists.get(id)})
    this._swapView(view);
  },

  _swapView: function(view) {
    this._currentView && this._currentView.remove();
    this._currentView = view;
    this.$rootEl.html(view.render().$el);
  }
});
