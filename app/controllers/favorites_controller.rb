class FavoritesController < ApplicationController

  def index
    @gists = current_user.favorited_gists
    render :json => @gists
  end

  def create
    @favorite = Favorite.new(params[:favorite])
    render :json => @favorite if @favorite.save
  end

  def destroy
    #@favorite = Favorite.find(params[:id])
  end
end
