class GistsController < ApplicationController

  # def gists_to_builder
  #   Jbuilder.new do |gists|
  #
  #
  #   end
  # end

  def index
    @gists = current_user.gists
    @gists
  end

  def create
    @gist = Gist.new(params[:gist])

    @gist.user_id = current_user.id
    @gist.gist_files.new(params[:gist_files].values)


    # @gist.tags.new(params[:tag].values)
    #when done with code review ask why this works.
    if @gist.save
      render :json => @gist
    else
      render :json => @gist.errors.full_mesages
    end
#gist_file.save!
  end

  def update
    @gist = Gist.find(params[:id])
    @gist.update_attributes(params[:gist])
    # @gist.gist_files.update_attributes(params[:gist_files].values)
    render :json => @gist
  end
end
