NewAuthDemo::Application.routes.draw do
  resources :users, :only => [:create, :new, :show] do
    resources :gists, :except => [:index]
    end
  resource :session, :only => [:create, :destroy, :new]
  resources :gists, :only => [:index, :create, :update] do
    resource :favorite, :only => [:create, :destroy]
  end
  resources :gist_files, :only => [:create, :update]
  resources :favorites, :only => [:index]
  resources :tags, :only => [:index]
  resource :root do
    collection do
      get :root
    end
  end
  root :to => "users#show"
end
